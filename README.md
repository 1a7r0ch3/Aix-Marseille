<table><tr>
<td width="50%"><img src="screenshots/beamerAMU-0.svg" width="100%"/></td>
<td width="50%"><img src="screenshots/beamerAMU-2.svg" width="100%"/></td>
</tr><tr>
<td width="50%"><img src="screenshots/beamerAMU-3.svg" width="100%"/></td>
<td width="50%"><img src="screenshots/beamerAMU-5.svg" width="100%"/></td>
</tr></table>

<table><tr valign="top"><td>

## Thème de l'université d'Aix-Marseille pour diaporamas et posters avec LaTeX
Les thèmes sont definies dans `beamerthemeAix-Marseille.sty` et `beamerposterthemeAix-Marseille.sty`.  
Les modèles d'utilisation sont `beamerAMU.ltx` et `posterAMU.ltx`.  
Les images nécessaires sont dans le dossier `graphics/`.  

### Polices d'écriture
La charte graphique d'AMU utilise la police d'écriture Klavika, relativement originale.
Ce n'est pas une police libre, mais on peut la trouver sur Internet.
Une fois que l'on a récupéré les quelques fichiers `klavika-*.otf` nécessaires, il suffit de lancer le formidable outil `autoinst`, avec ces fichiers en arguments, pour installer la police sur son système LaTeX.  
La police d'écriture mathématique est aussi Klavika.  
Les chiffres sont à l'ancienne en mode texte, et alignés en mode math.  
Les lettres grecques et les symbols sont empruntés à la police Computer Modern sans sérif.  
Les lettres imitation tableau noirs utilisent la police Doublestroke sans sérif.  
Les lettres calligraphiques utilisent la police ITC Zapf Chancery, mise à l'échelle avec le paquet `mathalfa`.  

### Divers
Le paquet `babel` avec l'option `french` n'est pas chargé par défaut dans le thème, mais est inclus dans les modèles.  
Les modèles incluent plusieurs commandes classiques dans le préambule, et les logotypes peuvent être aussi modifiés à cet endroit.  

</td><td>

## Aix-Marseille University theme for beamers and posters with LaTeX
Themes are defined in `beamerthemeAix-Marseille.sty` and `beamerposterthemeAix-Marseille.sty`.  
Templates are `beamerAMU.ltx` and `posterAMU.ltx`.  
Required graphics are in `graphics/`.  

### Typefaces
AMU uses the pretty fancy Klavika typeface. It is not a free typeface, but can be found over the Internet.
Once provided with a bunch of `klavika-*.otf` files, run the amazing `autoinst` tool on these files for installing the typeface on your LaTeX system (`texhash` and `updmap-sys` might be necessary to complete the installation).   
Math typeface is also set to Klavika.  
Old style figures are used in text mode, lining figures in math mode.  
Greeks and symbols are taken from the sans-serif Computer Modern typeface.  
Blackboard letters are set to sans-serif Doublestroke typeface.  
Calligraphic letters are set as Zapf Chancery and scaled with the `mathalfa` package.

### Misc
The `babel` package with option `french` is not loaded by default in the theme, but included in the template.  
The template includes several classical commands in the preambule; logotypes can be changed here as well.

</td></td></table>

<table cellpadding="0" cellspacing="0" border="0" width="100%"><tr><td align="center">
<img src="screenshots/posterAMU.svg" width="50%"/>
</td></tr></table>

